module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    typescript: {
      base: {
        src: ['QreaFacturation.ts', 'Models/*.ts', 'Helpers/*.ts', 'QreaFacturationExport.ts'],
        dest: 'src/<%= pkg.name %>.js',
        options: {
          module: 'amd', //or commonjs
          target: 'es5', //or es3
          basePath: 'path/to/typescript/files',
          sourceMap: true,
          declaration: true
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        mangle: false
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    },
    mochaTest: {
        test: {
            options: {
                reporter: 'spec',
                captureFile: 'results.txt', // Optionally capture the reporter output to a file
                quiet: false, // Optionally suppress output to standard out (defaults to false)
                clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false)
            },
            src: ['test/**/*.js']
        }
    },
    copy: {
      toQrea: {
        files: [
          // includes files within path
          { expand: true, flatten: true, src: ['build/qrea-facturation.min.js'], dest: '../qrea-app/client/js/qreaLib', filter: 'isFile' },
        ]
      }
    }
  });

  // Load grunt-typescript
  grunt.loadNpmTasks('grunt-typescript');

  // Load uglify
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Add the grunt-mocha-test tasks.
  grunt.loadNpmTasks('grunt-mocha-test');

  // add the copy feature
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Default task(s).
  grunt.registerTask('default', ['typescript', 'uglify', 'mochaTest']);
  grunt.registerTask('toQrea', ['default', 'copy:toQrea']);

};
