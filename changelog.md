#1.0.6
* Ajout du model FactureAcompte

#1.0.5
* Ajout de la propriété isFactureAcompte au model Facture

#1.0.4
* Mise en place de l'instanciation à partir des enregistrements JSON
~~~~
QreaFacturation.Models.MyModel.instanciate(params)
~~~~
Instancie le "MyModel" à partir de paramètres qui peuvent être une instance OU un objet JSON
* Gestion de l'instanciation des propriété de Model 
*exemple: propriété article du model vente*