/// <reference path="../QreaFacturation.ts" />
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var BaseModel = (function () {
            function BaseModel(o) {
            }
            // on override la fonction qui vient de baseModel
            BaseModel.instanciate = function (o) {
                if (!o)
                    return null;
                // on doit déterminé si c'est une vente ou non pour renvoyer un objet vente correctement instancié
                if (o['getName'] && o.getName() === this.getName()) {
                    // c'est ok on retourne la vente
                    return o; // TODO : voir si on peut décaller cette méthode directement dans le constructeur
                }
                else {
                    return new this(o);
                }
            };
            BaseModel.prototype.getName = function () {
                return this.constructor.name;
            };
            BaseModel.getName = function () {
                return this.constructor.name;
            };
            BaseModel.prototype.getPourcent = function (value) {
                return Math.round(value * 100 * 100) / 100;
            };
            BaseModel.prototype.round = function (value) {
                // arrondire à deux décimales
                var v = value * 100;
                v = Math.round(v);
                v = v / 100;
                return v;
            };
            // http://mattpolzin.com/?p=479
            BaseModel.prototype.watch = function (prop, handler) {
                var oldval = this[prop];
                var newval = oldval;
                var getter = function () {
                    return newval;
                };
                var setter = function (val) {
                    oldval = newval;
                    return newval = handler.call(this, prop, oldval, val);
                };
                if (delete this[prop]) {
                    if (Object.defineProperty) {
                        Object.defineProperty(this, prop, {
                            'get': getter,
                            'set': setter
                        });
                    }
                    else if (Object.prototype['__defineGetter__'] && Object.prototype['__defineSetter__']) {
                        Object.prototype['__defineGetter__'].call(this, prop, getter);
                        Object.prototype['__defineSetter__'].call(this, prop, setter);
                    }
                }
            };
            BaseModel.prototype.unwatch = function (prop) {
                var val = this[prop];
                delete this[prop]; // remove accessors
                this[prop] = val;
            };
            return BaseModel;
        })();
        Models.BaseModel = BaseModel;
        ;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var Adresse = (function (_super) {
            __extends(Adresse, _super);
            function Adresse(params) {
                _super.call(this, params);
                if (!params.nom && !params.cp && params.ville)
                    throw new Error('Le model \'Adresse\' requiert des paramètres \'nom\', \'cp\', \'ville\' non nuls');
                this.nom = params.nom;
                this.ligne1 = params.ligne1;
                this.ligne2 = params.ligne2 || null;
                this.cp = params.cp;
                this.ville = params.ville;
                this.pays = params.pays || null;
            }
            return Adresse;
        })(Models.BaseModel);
        Models.Adresse = Adresse;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var BaseArticle = (function (_super) {
            __extends(BaseArticle, _super);
            function BaseArticle(params) {
                _super.call(this, params);
                if (!params.libelle)
                    throw new Error('Le model \'BaseArticle\' requiert un paramètre \'libelle\' non nul');
                this.libelle = params.libelle;
                this.reference = params.reference;
            }
            BaseArticle.instanciateArticleOuGroupe = function (o) {
                if (o.qteArticles) {
                    // on créer un groupe d'articles
                    return QreaFacturation.Models.GroupeArticles.instanciate(o);
                }
                else {
                    return QreaFacturation.Models.Article.instanciate(o);
                }
            };
            return BaseArticle;
        })(Models.BaseModel);
        Models.BaseArticle = BaseArticle;
        var QteArticles = (function (_super) {
            __extends(QteArticles, _super);
            function QteArticles(params) {
                _super.call(this, params);
                this.quantite = params.quantite;
                this.article = params.article;
            }
            Object.defineProperty(QteArticles.prototype, "total", {
                get: function () {
                    if (!this.quantite || !this.article || !this.article.prix) {
                        console.warn('Impossible de calculer le total...');
                        return 0;
                    }
                    return this.quantite * this.article.prix;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(QteArticles.prototype, "article", {
                get: function () {
                    return this._article;
                },
                set: function (a) {
                    this._article = QreaFacturation.Models.Article.instanciate(a);
                },
                enumerable: true,
                configurable: true
            });
            return QteArticles;
        })(Models.BaseModel);
        Models.QteArticles = QteArticles;
        var GroupeArticles = (function (_super) {
            __extends(GroupeArticles, _super);
            function GroupeArticles(params) {
                _super.call(this, params);
                if (!params.qteArticles)
                    throw new Error('Le model \'GroupeArticles\' requiert une propriété \'qteArticles\' valide');
                this.qteArticles = params.qteArticles;
            }
            Object.defineProperty(GroupeArticles.prototype, "qteArticles", {
                get: function () {
                    return this._qteArticles;
                },
                set: function (articles) {
                    this._qteArticles ? this._qteArticles.length = 0 : this._qteArticles = [];
                    if (Array.isArray(articles)) {
                        for (var i = 0; i < articles.length; i++) {
                            var element = articles[i];
                            this._qteArticles.push(QreaFacturation.Models.QteArticles.instanciate(element));
                        }
                    }
                    else {
                        this._qteArticles.push(QreaFacturation.Models.QteArticles.instanciate(articles));
                    }
                },
                enumerable: true,
                configurable: true
            });
            GroupeArticles.prototype.addQteArticles = function (q) {
                this._qteArticles.push(QreaFacturation.Models.QteArticles.instanciate(q));
            };
            Object.defineProperty(GroupeArticles.prototype, "prix", {
                get: function () {
                    var p = 0;
                    for (var i = 0; i < this.qteArticles.length; i++) {
                        var q = this.qteArticles[i];
                        p += q.total;
                    }
                    return p;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GroupeArticles.prototype, "tauxTVA", {
                get: function () {
                    var totalQte = 0;
                    var somme = 0;
                    for (var i = 0; i < this.qteArticles.length; i++) {
                        var q = this.qteArticles[i];
                        totalQte += q.quantite;
                        somme += q.article.tauxTVA * q.quantite;
                    }
                    var tauxPondere = somme / totalQte * 100;
                    var tauxArrondi = this.round(tauxPondere);
                    return tauxArrondi / 100;
                },
                enumerable: true,
                configurable: true
            });
            return GroupeArticles;
        })(BaseArticle);
        Models.GroupeArticles = GroupeArticles;
        var Article = (function (_super) {
            __extends(Article, _super);
            function Article(params) {
                _super.call(this, params);
                //this.libelle = params.libelle;
                this.unite = params.unite || null;
                this.prix = params.prix;
                this.tauxTVA = params.tauxTVA || 0;
                //this.reference = params.reference || null;
            }
            return Article;
        })(BaseArticle);
        Models.Article = Article;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
var QreaFacturation;
(function (QreaFacturation) {
    var Helpers;
    (function (Helpers) {
        var DocumentDefinitionObjectHelper = (function () {
            function DocumentDefinitionObjectHelper() {
            }
            DocumentDefinitionObjectHelper.getDDO = function (templateName) {
                return {
                    content: this.getContentTest(),
                    styles: this.getStylesTest()
                };
            };
            DocumentDefinitionObjectHelper.getStylesTest = function () {
                return {
                    header: {
                        fontSize: 18,
                        bold: true,
                        alignment: 'right',
                        margin: [0, 190, 0, 80]
                    },
                    subheader: {
                        fontSize: 14
                    },
                    superMargin: {
                        margin: [20, 0, 40, 0],
                        fontSize: 15,
                    }
                };
            };
            DocumentDefinitionObjectHelper.getContentTest = function () {
                return [
                    {
                        stack: [
                            'This header has both top and bottom margins defined',
                            { text: 'This is a subheader', style: 'subheader' },
                        ],
                        style: 'header'
                    },
                    {
                        text: [
                            'Margins have slightly different behavior than other layout properties. They are not inherited, unlike anything else. They\'re applied only to those nodes which explicitly ',
                            'set margin or style property.\n',
                        ]
                    },
                    {
                        text: 'This paragraph (consisting of a single line) directly sets top and bottom margin to 20',
                        margin: [0, 20],
                    },
                    {
                        stack: [
                            { text: [
                                    'This line begins a stack of paragraphs. The whole stack uses a ',
                                    { text: 'superMargin', italics: true },
                                    ' style (with margin and fontSize properties).',
                                ]
                            },
                            { text: ['When you look at the', { text: ' document definition', italics: true }, ', you will notice that fontSize is inherited by all paragraphs inside the stack.'] },
                            'Margin however is only applied once (to the whole stack).'
                        ],
                        style: 'superMargin'
                    },
                    {
                        stack: [
                            'I\'m not sure yet if this is the desired behavior. I find it a better approach however. One thing to be considered in the future is an explicit layout property called inheritMargin which could opt-in the inheritance.\n\n',
                            {
                                fontSize: 15,
                                text: [
                                    'Currently margins for ',
                                    /* the following margin definition doesn't change anything */
                                    { text: 'inlines', margin: 20 },
                                    ' are ignored\n\n'
                                ],
                            },
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.\n',
                        ],
                        margin: [0, 20, 0, 0],
                        alignment: 'justify'
                    }
                ];
            };
            return DocumentDefinitionObjectHelper;
        })();
        Helpers.DocumentDefinitionObjectHelper = DocumentDefinitionObjectHelper;
    })(Helpers = QreaFacturation.Helpers || (QreaFacturation.Helpers = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />
/// <reference path="../Helpers/DocumentDefinitionObjectHelper.ts"/>
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var Document = (function (_super) {
            __extends(Document, _super);
            function Document(params) {
                _super.call(this, params);
                //this.typeDocument = null;
                this.libelle = params.libelle || null;
                this.date = params.date || Date.now();
                this.numero = params.numero || null;
                this.entreprise = params.entreprise;
                this.client = params.client;
                this.ventes = params.ventes || [];
                this.prctRemiseGlobale = params.prctRemiseGlobale || 0;
                this.isAutoliquidation = params.isAutoliquidation || false;
                this.reglements = params.reglements || [];
                // this.logoEntreprise = params.logoEntreprise || null;
                this.adresseLivraison = params.adresseLivraison || null;
                this.details = params.details || null;
                this.logo = params.logo || null;
                this.detailsTVA = {};
                if (this['calculate'])
                    this.calculate();
            }
            Document.prototype.calculate = function () {
                var self = this;
                var d = {};
                self._totalHT = 0;
                self._totalTTC = 0;
                self._totalTVA = 0;
                // TODO : facture d'acompte => scinder par taux de tva pour dans le calcul du details
                if (self.ventes && Array.isArray(self.ventes)) {
                    self.ventes.forEach(function (vente) {
                        self._totalHT += vente.totalHT;
                        self._totalTVA += vente.totalTVA;
                        self._totalTTC += vente.totalTTC;
                        //if(!vente.article) return;
                        // si le taux n'existe pas dans le calcul du details
                        if (!d[vente.article.tauxTVA.toString()]) {
                            var details = new DetailsTVA(vente.article.tauxTVA);
                            d[vente.article.tauxTVA.toString()] = details;
                        }
                        // ajout de la base
                        d[vente.article.tauxTVA.toString()].base += vente.totalHT;
                    });
                }
                // ON arrondire
                self._totalTVA = self.round(self._totalTVA);
                self._totalHT = self.round(self._totalHT);
                self._totalTTC = self.round(self._totalTTC);
                self.detailsTVA = d;
                calculateNetAPayer();
                // calculer le net à payer de la facture
                function calculateNetAPayer() {
                    var regle = 0; // le total deja payé
                    // si les règlements ont été définis
                    if (Array.isArray(self.reglements)) {
                        self.reglements.forEach(function (r) {
                            if (r.paye === true)
                                regle = r.montant;
                        });
                    }
                    // on stock le res
                    self._netAPayer = self.round(self._totalTTC - regle);
                }
            };
            Document.prototype.removeVenteByIndex = function (index) {
                this.ventes.splice(index, 1);
                this.calculate();
            };
            Document.prototype.addReglement = function (newReglt) {
                this.reglements.push(QreaFacturation.Models.Reglement.instanciate(newReglt));
                this.calculate();
            };
            Document.prototype.removeReglementByIndex = function (index) {
                this.reglements.splice(index, 1);
                this.calculate();
            };
            /**
             * toDDO - Convertit le document DocumentDefinitionObject conforme
             * à la libraire pdfMake https://github.com/bpampuch/pdfmake
             *
             * @return {object}  ddo
             */
            Document.prototype.toDDO = function () {
                var ddo = QreaFacturation.Helpers.DocumentDefinitionObjectHelper.getDDO('template1');
                return ddo;
            };
            Object.defineProperty(Document.prototype, "totalHT", {
                get: function () {
                    this.calculate();
                    return this._totalHT;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Document.prototype, "totalTTC", {
                get: function () {
                    this.calculate();
                    return this._totalTTC;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Document.prototype, "totalTVA", {
                get: function () {
                    this.calculate();
                    return this._totalTVA;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Document.prototype, "netAPayer", {
                get: function () {
                    this.calculate();
                    return this._netAPayer;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Document.prototype, "entreprise", {
                get: function () {
                    return this._entreprise;
                },
                set: function (e) {
                    this._entreprise = QreaFacturation.Models.Entreprise.instanciate(e);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Document.prototype, "logo", {
                get: function () {
                    return this._logo;
                },
                set: function (l) {
                    this._logo = QreaFacturation.Models.Logo.instanciate(l);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Document.prototype, "client", {
                get: function () {
                    return this._client;
                },
                set: function (c) {
                    this._client = QreaFacturation.Models.Personne.instanciatePhysiqueOuMorale(c);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Document.prototype, "reglements", {
                get: function () {
                    return this._reglements;
                },
                set: function (reglts) {
                    if (!reglts)
                        return;
                    this._reglements ? this._reglements.length = 0 : this._reglements = [];
                    if (Array.isArray(reglts)) {
                        for (var i = 0; i < reglts.length; i++) {
                            var element = reglts[i];
                            this._reglements.push(QreaFacturation.Models.Reglement.instanciate(element));
                        }
                    }
                    else {
                        this._reglements.push(QreaFacturation.Models.Reglement.instanciate(reglts));
                    }
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Document.prototype, "ventes", {
                get: function () {
                    return this._ventes;
                },
                set: function (ventes) {
                    if (!ventes)
                        return;
                    this._ventes ? this._ventes.length = 0 : this._ventes = []; // reset du tableau;
                    // on vérifie si c'est un seul object ou un tableau
                    if (Array.isArray(ventes)) {
                        for (var i = 0; i < ventes.length; i++) {
                            var element = ventes[i];
                            this._ventes.push(QreaFacturation.Models.Vente.instanciate(element));
                        }
                    }
                    else {
                        this._ventes.push(QreaFacturation.Models.Vente.instanciate(ventes));
                    }
                },
                enumerable: true,
                configurable: true
            });
            Document.prototype.addVente = function (newVente) {
                this._ventes.push(QreaFacturation.Models.Vente.instanciate(newVente));
                this.calculate();
            };
            Object.defineProperty(Document.prototype, "adresseLivraison", {
                get: function () {
                    return this._adresseLivraison;
                },
                set: function (a) {
                    this._adresseLivraison = QreaFacturation.Models.Adresse.instanciate(a);
                },
                enumerable: true,
                configurable: true
            });
            return Document;
        })(Models.BaseModel);
        Models.Document = Document;
        var DetailsTVA = (function (_super) {
            __extends(DetailsTVA, _super);
            function DetailsTVA(taux, base) {
                _super.call(this, {});
                this._taux = taux;
                this._base = base || 0;
                this.calculate();
            }
            DetailsTVA.prototype.calculate = function () {
                this.tva = this._taux * this._base;
                this.tva = this.round(this.tva);
            };
            Object.defineProperty(DetailsTVA.prototype, "taux", {
                get: function () {
                    return this._taux;
                },
                set: function (newValue) {
                    this._taux = newValue;
                    this.calculate();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DetailsTVA.prototype, "base", {
                get: function () {
                    return this._base;
                },
                set: function (newValue) {
                    this._base = newValue;
                    this.calculate();
                },
                enumerable: true,
                configurable: true
            });
            return DetailsTVA;
        })(Models.BaseModel);
        Models.DetailsTVA = DetailsTVA;
        var Facture = (function (_super) {
            __extends(Facture, _super);
            function Facture(params) {
                _super.call(this, params);
            }
            return Facture;
        })(Document);
        Models.Facture = Facture;
        var FactureAcompte = (function (_super) {
            __extends(FactureAcompte, _super);
            function FactureAcompte(params) {
                _super.call(this, params);
                this.acompteHT = params.acompteHT || 0;
                this.acompteTVA = params.acompteTVA || 0;
            }
            Object.defineProperty(FactureAcompte.prototype, "acompteTTC", {
                get: function () {
                    return this.acompteHT + this.acompteTVA;
                },
                enumerable: true,
                configurable: true
            });
            return FactureAcompte;
        })(Document);
        Models.FactureAcompte = FactureAcompte;
        var Devis = (function (_super) {
            __extends(Devis, _super);
            function Devis(params) {
                _super.call(this, params);
                this.dateValidite = params.dateValidite;
            }
            return Devis;
        })(Document);
        Models.Devis = Devis;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var Entreprise = (function (_super) {
            __extends(Entreprise, _super);
            function Entreprise(params) {
                _super.call(this, params);
                if (!params.personne)
                    throw new Error('Le model \'Entreprise\' requiert une propriété \'personne\' valide');
                this.personne = params.personne;
                this.isAdherentCGA = params.isAdherentCGA;
                this.isExonere = params.isExonere;
                this.isAssujettiTVA = params.isAssujettiTVA;
                this.isFranchiseEnBase = params.isFranchiseEnBase;
                this.isRegimeMargeBeneficiaire = params.isRegimeMargeBeneficiaire;
                this.isAutoliquidation = params.isAutoliquidation;
                this.identification = params.identification;
                this.numeroTVA = params.numeroTVA;
                this.capital = params.capital;
                this.isCapitalVariable = params.isCapitalVariable;
                this.tauxPenalitesReglement = params.tauxPenalitesReglement;
                this.conditionsEscompte = params.conditionsEscompte;
                this.mentionsParticulieres = params.mentionsParticulieres;
                this.modeReglementDefaut = params.modeReglementDefaut;
            }
            Object.defineProperty(Entreprise.prototype, "personne", {
                // on peut avoir une personne physique ou morale
                get: function () {
                    return this._personne;
                },
                set: function (p) {
                    this._personne = QreaFacturation.Models.Personne.instanciatePhysiqueOuMorale(p);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Entreprise.prototype, "identification", {
                get: function () {
                    return this._identification;
                },
                set: function (i) {
                    this._identification = QreaFacturation.Models.Identification.instanciate(i);
                },
                enumerable: true,
                configurable: true
            });
            return Entreprise;
        })(Models.BaseModel);
        Models.Entreprise = Entreprise;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var Identification = (function (_super) {
            __extends(Identification, _super);
            function Identification(params) {
                _super.call(this, params);
                this.SIREN = params.SIREN;
                this.NIC = params.NIC;
                this.APE = params.APE;
                this.RCS = params.RCS || null;
                this.RM = params.RM || null;
                // ON CONSTRUIT LE SIRET
                this.SIRET = this.SIREN + this.NIC;
            }
            return Identification;
        })(Models.BaseModel);
        Models.Identification = Identification;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var Logo = (function (_super) {
            __extends(Logo, _super);
            function Logo(params) {
                _super.call(this, params);
                this.base64 = params.base64 || "";
            }
            return Logo;
        })(Models.BaseModel);
        Models.Logo = Logo;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var Personne = (function (_super) {
            __extends(Personne, _super);
            function Personne(params) {
                _super.call(this, params);
                this.adresse = params.adresse;
                this.isEntreprise = params.isEntreprise || false;
                this.numeroTVA = params.numeroTVA || null;
                this.telephone = params.telephone || null;
                this.email = params.email || null;
                this.siteInternet = params.siteInternet || null;
                this.fax = params.fax || null;
                this.identification = params.identification || null;
            }
            Personne.instanciatePhysiqueOuMorale = function (newPersonne) {
                if (!newPersonne.denominationSociale) {
                    // on instancie une personne physique
                    return QreaFacturation.Models.PersonnePhysique.instanciate(newPersonne);
                }
                else {
                    return QreaFacturation.Models.PersonneMorale.instanciate(newPersonne);
                }
            };
            Object.defineProperty(Personne.prototype, "adresse", {
                get: function () {
                    return this._adresse;
                },
                set: function (a) {
                    this._adresse = QreaFacturation.Models.Adresse.instanciate(a);
                },
                enumerable: true,
                configurable: true
            });
            return Personne;
        })(Models.BaseModel);
        Models.Personne = Personne;
        var PersonnePhysique = (function (_super) {
            __extends(PersonnePhysique, _super);
            function PersonnePhysique(params) {
                _super.call(this, params);
                this.civilite = params.civilite || null;
                this.nom = params.nom || null;
                this.prenom = params.prenom || null;
                this.nomCommercial = params.nomCommercial || null;
            }
            return PersonnePhysique;
        })(Personne);
        Models.PersonnePhysique = PersonnePhysique;
        var PersonneMorale = (function (_super) {
            __extends(PersonneMorale, _super);
            function PersonneMorale(params) {
                _super.call(this, params);
                this.forme = params.forme;
                this.denominationSociale = params.denominationSociale;
                this.capitalSocial = params.capitalSocial || null;
                this.isCapitalVariable = params.isCapitalVariable || false;
            }
            return PersonneMorale;
        })(Personne);
        Models.PersonneMorale = PersonneMorale;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var Reglement = (function (_super) {
            __extends(Reglement, _super);
            function Reglement(params) {
                _super.call(this, params);
                //this.type = params.type;
                this.delai = params.delai;
                //this.moyen = params.moyen;
                this.montant = params.montant;
                this.pourcentage = params.pourcentage;
                this.finDeMois = params.finDeMois;
                this.paye = params.paye;
            }
            return Reglement;
        })(Models.BaseModel);
        Models.Reglement = Reglement;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />
var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        var Vente = (function (_super) {
            __extends(Vente, _super);
            function Vente(params) {
                _super.call(this, params);
                if (!params.article)
                    throw new Error('L\'objet vente requiert une propriété \'article\' valide');
                if (!params.quantite)
                    throw new Error('L\'objet vente requiert une propriété \'quantite\'');
                this.article = params.article;
                this.quantite = params.quantite;
                this.prctRemise = params.prctRemise || null;
                this.calculate();
            }
            Vente.prototype.calculate = function () {
                // si les infos nécessaires au calcul ne sont pas donnée on set tout à 0 et on arrête
                if (!this.article || !this._quantite) {
                    this._totalHT = 0;
                    this._totalTTC = 0;
                    this._totalTVA = 0;
                    return;
                }
                else {
                    this._totalHT = this.article.prix * this._quantite;
                    if (this.prctRemise) {
                        this._totalHT = this._totalHT * (1 - this.prctRemise);
                    }
                    this._totalHT = this.round(this._totalHT);
                    this._totalTVA = this.round(this._totalHT * this.article.tauxTVA);
                    this._totalTTC = this._totalTVA + this._totalHT;
                }
            };
            Object.defineProperty(Vente.prototype, "article", {
                get: function () {
                    return this._article;
                },
                set: function (a) {
                    // on instancie un article ou un groupe d'articles
                    this._article = Models.BaseArticle.instanciateArticleOuGroupe(a);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Vente.prototype, "quantite", {
                get: function () {
                    return this._quantite;
                },
                set: function (newQuantite) {
                    this._quantite = newQuantite;
                    this.calculate();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Vente.prototype, "totalHT", {
                get: function () {
                    this.calculate();
                    return this._totalHT;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Vente.prototype, "totalTTC", {
                get: function () {
                    this.calculate();
                    return this._totalTTC;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Vente.prototype, "totalTVA", {
                get: function () {
                    this.calculate();
                    return this._totalTVA;
                },
                enumerable: true,
                configurable: true
            });
            return Vente;
        })(Models.BaseModel);
        Models.Vente = Vente;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
var module = module;
module ? module.exports = QreaFacturation : null;
//# sourceMappingURL=qrea-facturation.js.map