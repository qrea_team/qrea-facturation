var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        class Personne extends Models.BaseModel {
            constructor(params) {
                super(params);
                this.adresse = params.adresse;
                this.isEntreprise = params.isEntreprise;
                this.numeroTVA = params.numeroTVA;
                this.telephone = params.telephone;
                this.email = params.email;
                this.siteInternet = params.siteInternet;
                this.fax = params.fax;
            }
        }
        Models.Personne = Personne;
        class PersonnePhysique extends Personne {
            constructor(params) {
                super(params);
                this.civilite = params.civilite || null;
                this.nom = params.nom || null;
                this.prenom = params.prenom || null;
                this.nomCommercial = params.nomCommercial || null;
            }
        }
        Models.PersonnePhysique = PersonnePhysique;
        class PersonneMorale extends Personne {
            constructor(params) {
                super(params);
                this.forme = params.forme;
                this.denominationSociale = params.denominationSociale;
                this.capitalSocial = params.capitalSocial || null;
                this.isCapitalVariable = params.isCapitalVariable || false;
            }
        }
        Models.PersonneMorale = PersonneMorale;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
