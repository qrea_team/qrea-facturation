var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        class Article extends Models.BaseModel {
            constructor(params) {
                super(params);
                this.libelle = params.libelle;
                this.unite = params.unite || null;
                this.prix = params.prix;
                this.tauxTVA = params.tauxTVA;
            }
        }
        Models.Article = Article;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
