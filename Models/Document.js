var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        class Document extends Models.BaseModel {
            constructor(params) {
                super(params);
                this.libelle = params.libelle;
                this.date = params.date || Date.now();
                this.numero = params.numero;
                this.entreprise = params.entreprise;
                this.client = params.client;
                this.ventes = params.ventes || [];
                this.prctRemiseGlobale = params.prctRemiseGlobale || 0;
                this.isAutoliquidation = params.isAutoliquidation;
                this.reglements = params.reglements;
                this.logoEntreprise = params.logoEntreprise;
                this.adresseLivraison = params.adresseLivraison;
            }
            toDDO() {
                var ddo = QreaFacturation.Helpers.DocumentDefinitionObjectHelper.getDDO('template1');
            }
        }
        Models.Document = Document;
        class Facture extends Document {
            constructor(params) {
                super(params);
                this.adresseLivraison = params.adresseLivraison;
            }
        }
        Models.Facture = Facture;
        class Devis extends Models.Personne {
            constructor(params) {
                super(params);
                this.dateValidite = params.dateValidite;
            }
        }
        Models.Devis = Devis;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
