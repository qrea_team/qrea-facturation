
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />

module QreaFacturation {

  export module Models {

    export class Reglement extends BaseModel {

      constructor(params: any) {

        super(params);

        //this.type = params.type;
        this.delai = params.delai;
        //this.moyen = params.moyen;
        this.montant = params.montant;
        this.pourcentage = params.pourcentage;
        this.finDeMois = params.finDeMois;
        this.paye = params.paye;

      }

      delai: number; //en jours
      montant: number;
      pourcentage: number;
      finDeMois: boolean;
      paye: boolean;

    }

  }

}
