var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        class Adresse extends Models.BaseModel {
            constructor(params) {
                super(params);
                this.ligne1 = params.ligne1;
                this.ligne2 = params.ligne2 || null;
                this.cp = params.cp;
                this.ville = params.ville;
                this.pays = params.pays || null;
            }
        }
        Models.Adresse = Adresse;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
