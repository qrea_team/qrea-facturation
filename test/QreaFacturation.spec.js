﻿var assert = require('chai').assert;

describe('QreaFacturation', function () {

  describe('Models', function(){

    var QreaFacturation = require('../src/qrea-facturation');

    // adresse client
    var adresseClient;

    // adresse entreprise
    var adresseEntreprise;

    // client
    var client;

    // identification
    var identificationEntreprise;

    // entreprise
    var entreprise;

    // articles
    var articles;

    // ventes
    var ventes;

    // facture
    var facture;

    it('should instanciate models from JSON', function(){
      
      var doc2 = new QreaFacturation.Models.Facture({
        entreprise: new QreaFacturation.Models.Entreprise({
          isAdherentCGA: false,
          personne: {
            nom: 'Bourdu',
            prenom: 'Pierre'
          }
        }),
        client: new QreaFacturation.Models.PersonneMorale({
          denominationSociale: 'QREA'
        }),
        ventes: {
          quantite: 2,
          article: {
            prix: 12,
            libelle: 'mon article',
            tauxTVA: 0.2
          }
        }
      });
      
      assert.equal(doc2.entreprise.getName(), 'Entreprise');
      assert.equal(doc2.ventes[0].getName(), 'Vente');
      assert.equal(doc2.totalHT, 24);

    })

    it('sould instanciate an Adresse', function () {

      adresseClient = new QreaFacturation.Models.Adresse({
        ligne1: '19 rue de la Tranchée',
        cp: '86000',
        ville: 'poitiers'
      });

      assert.isDefined(adresseClient, 'erreur pendant l\'instanciation de l\'adresse');
      assert.equal(adresseClient.ville, 'poitiers', 'erreur sur la ville');

    });

    it('should inscanciate a Logo', function(){

        var logo = new QreaFacturation.Models.Logo({
          base64: 'data:dsfjmlkdsjfmjlds',
          
        });
        assert.isDefined(logo);
        

    });

    it('should instanciate a Personne Physique', function(){

      var p = {
        adresse: adresseClient,
        nom: 'Dujardin',
        prenom: 'Jean',
      };

      client = new QreaFacturation.Models.PersonnePhysique(p);
      assert.isDefined(client);
      assert.equal(client.nom, 'Dujardin');
      assert.isNotOk(client.isEntreprise);

    });

    it('should instanciate an Entreprise', function(){

      var adresseEntreprise = new QreaFacturation.Models.Adresse({
        ligne1: '17 rue Henry Monnier',
        cp: '75009',
        ville: 'PARIS'
      });

      var i = new QreaFacturation.Models.Identification({
        SIREN: '123456789',
        NIC: '00001',
        APE: 'AZE03',
        RCS: 'Paris'
      });

      var e = {
        personne: {
          denominationSociale: 'QREA'
        },
        adresse: adresseEntreprise,
        identification: i,
        isEntreprise: true,
        forme: 'SAS',
        denominationSociale: 'Qrea',
        isCapitalVariable: true,
        capitalSocial: 16000
      };

      entreprise = new QreaFacturation.Models.Entreprise(e);
      assert.isDefined(entreprise);
      assert.isOk(entreprise.isCapitalVariable);
      assert.equal(entreprise.identification.SIREN, '123456789');

    });

    it('should instanciate an Article list', function(){

      var art1 = new QreaFacturation.Models.Article({
        libelle: 'mon article 1',
        prix: 12,
        tauxTVA: 0.2
      });

      var art2 = new QreaFacturation.Models.Article({
        libelle: 'mon article 2',
        prix: 15.36,
        tauxTVA: 0.2
      });

      articles = [art1, art2];

      assert.isNull(art1.unite);

    });

    it('should instanciate an Vente list', function(){

      var vente1 = new QreaFacturation.Models.Vente({
        article: articles[0],
        quantite: 5
      });
      var vente2 = new QreaFacturation.Models.Vente({
        article: articles[1],
        quantite: 7,
        prctRemise: 0.25
      });

      assert.isNull(vente1.prctRemise);
      assert.equal(vente1.totalHT, 60, 'le totalHT de la vente est erroné');
      assert.equal(vente1.totalTVA, 12, 'le totalTVA de la vente est erroné');
      assert.equal(vente1.totalTTC, 72, 'le totalTTC de la vente est erroné');
      assert.equal(vente2.totalHT, 80.64, 'le totalHT remisé est faux');

      vente2.quantite = 6;
      assert.equal(vente2.totalHT, 69.12);

      vente2.article.prix = 10;
      assert.equal(vente2.totalHT, 45);

      vente2.article.prix = 15.36;

      ventes = [vente1, vente2];

    });

    it('should instanciate Facture', function(){

      var f = {
        libelle: 'ma facture',
        entreprise: entreprise,
        client: client,
        ventes: ventes
      };

      var fact = new QreaFacturation.Models.Facture(f);
      assert.isDefined(fact);
      assert.equal(fact.totalHT, 129.12);

      // on vérifie que les résultats calculés suivent les modification des éléments
      fact.ventes[0].quantite--;
      assert.equal(fact.totalHT, 129.12 - 12);

      // expose la facture pour la suite des tests
      facture = fact;

    });

    it('should match detailsTVA with total TVA', function(){

      assert.isNotNull(facture.detailsTVA);

      var totalCalcule = 0;
      for (var k in facture.detailsTVA) {

        if (facture.detailsTVA.hasOwnProperty(k)) {
          totalCalcule += facture.detailsTVA[k].tva;
        }

      }

      assert.equal(totalCalcule, facture.totalTVA);

    });

    it('should create a GroupeArticles', function(){

      var a1 = new QreaFacturation.Models.Article({
        libelle: 'article 1',
        prix: 10,
        tauxTVA: 0.1
      });

      var g = new QreaFacturation.Models.GroupeArticles({
        qteArticles: [
          new QreaFacturation.Models.QteArticles({
            quantite: 4,
            article: a1
          })
        ],
        libelle: 'groupe articles'
      });

      assert.equal(a1.prix, 10);

      assert.equal(g.prix, 40);

      var vente = new QreaFacturation.Models.Vente({
        article : g,
        quantite: 2
      });

      assert.equal(vente.totalHT, 80);

      assert.equal(g.tauxTVA, 0.1);

    });

    it('should instanciate a facture d\'acompte', function(){

      var f = new QreaFacturation.Models.FactureAcompte({
        date: new Date(),
        isFactureAcompte: true,
        details: 'Details de la facture',
        acompteHT: 100,
        acompteTVA: 20,
        entreprise: new QreaFacturation.Models.Entreprise({
          isAdherentCGA: false,
          personne: {
            nom: 'Bourdu',
            prenom: 'Pierre'
          }
        }),
        client: new QreaFacturation.Models.PersonneMorale({
          denominationSociale: 'QREA'
        })
      });

      assert.equal(f.acompteTTC, 120);

    });

  });

});
