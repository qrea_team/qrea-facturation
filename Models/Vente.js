var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        class Vente extends Models.BaseModel {
            constructor(params) {
                super(params);
                this.article = params.article;
                this.quantite = params.quantite;
                this.prctRemise = params.prctRemise;
            }
        }
        Models.Vente = Vente;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
