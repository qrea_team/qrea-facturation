/// <reference path="../QreaFacturation.d.ts" />
declare module QreaFacturation {
    module Models {
        class BaseModel {
            constructor(o: any);
            static instanciate(o: any): any;
            getName(): any;
            static getName(): any;
            protected getPourcent(value: number): number;
            protected round(value: number): number;
            watch(prop: string, handler: Function): void;
            unwatch(prop: string): void;
        }
    }
}
declare module QreaFacturation {
    module Models {
        class Adresse extends BaseModel {
            constructor(params: any);
            nom: string;
            ligne1: string;
            ligne2: string;
            cp: string;
            ville: string;
            pays: string;
        }
    }
}
declare module QreaFacturation {
    module Models {
        interface IArticle {
            libelle: string;
            prix: number;
            reference: string;
            tauxTVA: number;
        }
        class BaseArticle extends BaseModel {
            constructor(params: any);
            libelle: string;
            reference: string;
            static instanciateArticleOuGroupe(o: any): any;
        }
        class QteArticles extends BaseModel {
            constructor(params: any);
            quantite: number;
            total: number;
            private _article;
            article: Article;
        }
        class GroupeArticles extends BaseArticle implements IArticle {
            constructor(params: any);
            private _qteArticles;
            qteArticles: Array<QteArticles>;
            addQteArticles(q: any): void;
            prix: number;
            tauxTVA: number;
        }
        class Article extends BaseArticle implements IArticle {
            constructor(params: any);
            unite: string;
            prix: number;
            tauxTVA: number;
        }
    }
}
declare module QreaFacturation {
    module Helpers {
        class DocumentDefinitionObjectHelper {
            static getDDO(templateName: string): {
                content: ({
                    stack: (string | {
                        text: string;
                        style: string;
                    })[];
                    style: string;
                } | {
                    text: string[];
                } | {
                    text: string;
                    margin: number[];
                } | {
                    stack: ({
                        text: (string | {
                            text: string;
                            italics: boolean;
                        })[];
                    } | string)[];
                    style: string;
                } | {
                    stack: (string | {
                        fontSize: number;
                        text: (string | {
                            text: string;
                            margin: number;
                        })[];
                    })[];
                    margin: number[];
                    alignment: string;
                })[];
                styles: {
                    header: {
                        fontSize: number;
                        bold: boolean;
                        alignment: string;
                        margin: number[];
                    };
                    subheader: {
                        fontSize: number;
                    };
                    superMargin: {
                        margin: number[];
                        fontSize: number;
                    };
                };
            };
            static getStylesTest(): {
                header: {
                    fontSize: number;
                    bold: boolean;
                    alignment: string;
                    margin: number[];
                };
                subheader: {
                    fontSize: number;
                };
                superMargin: {
                    margin: number[];
                    fontSize: number;
                };
            };
            static getContentTest(): ({
                stack: (string | {
                    text: string;
                    style: string;
                })[];
                style: string;
            } | {
                text: string[];
            } | {
                text: string;
                margin: number[];
            } | {
                stack: ({
                    text: (string | {
                        text: string;
                        italics: boolean;
                    })[];
                } | string)[];
                style: string;
            } | {
                stack: (string | {
                    fontSize: number;
                    text: (string | {
                        text: string;
                        margin: number;
                    })[];
                })[];
                margin: number[];
                alignment: string;
            })[];
            constructor();
        }
    }
}
declare module QreaFacturation {
    module Models {
        abstract class Document extends BaseModel {
            constructor(params: any);
            protected calculate(): void;
            removeVenteByIndex(index: number): void;
            addReglement(newReglt: Reglement): void;
            removeReglementByIndex(index: number): void;
            /**
             * toDDO - Convertit le document DocumentDefinitionObject conforme
             * à la libraire pdfMake https://github.com/bpampuch/pdfmake
             *
             * @return {object}  ddo
             */
            toDDO(): {
                content: ({
                    stack: (string | {
                        text: string;
                        style: string;
                    })[];
                    style: string;
                } | {
                    text: string[];
                } | {
                    text: string;
                    margin: number[];
                } | {
                    stack: ({
                        text: (string | {
                            text: string;
                            italics: boolean;
                        })[];
                    } | string)[];
                    style: string;
                } | {
                    stack: (string | {
                        fontSize: number;
                        text: (string | {
                            text: string;
                            margin: number;
                        })[];
                    })[];
                    margin: number[];
                    alignment: string;
                })[];
                styles: {
                    header: {
                        fontSize: number;
                        bold: boolean;
                        alignment: string;
                        margin: number[];
                    };
                    subheader: {
                        fontSize: number;
                    };
                    superMargin: {
                        margin: number[];
                        fontSize: number;
                    };
                };
            };
            libelle: string;
            date: Date;
            numero: string;
            prctRemiseGlobale: number;
            isAutoliquidation: boolean;
            details: string;
            private _totalHT;
            totalHT: number;
            private _totalTTC;
            totalTTC: number;
            private _totalTVA;
            totalTVA: number;
            private _netAPayer;
            netAPayer: number;
            private _entreprise;
            entreprise: Entreprise;
            private _logo;
            logo: Logo;
            private _client;
            client: Personne;
            private _reglements;
            reglements: Array<Reglement>;
            private _ventes;
            ventes: Array<Vente>;
            addVente(newVente: any): void;
            private _adresseLivraison;
            adresseLivraison: Adresse;
            detailsTVA: any;
        }
        class DetailsTVA extends BaseModel {
            constructor(taux: number, base?: number);
            private calculate();
            _taux: number;
            taux: number;
            _base: number;
            base: number;
            tva: number;
        }
        class Facture extends Document {
            constructor(params: any);
        }
        class FactureAcompte extends Document {
            constructor(params: any);
            acompteHT: number;
            acompteTVA: number;
            acompteTTC: number;
        }
        class Devis extends Document {
            constructor(params: any);
            dateValidite: Date;
        }
    }
}
declare module QreaFacturation {
    module Models {
        class Entreprise extends BaseModel {
            constructor(params: any);
            isAdherentCGA: boolean;
            isExonere: boolean;
            isAssujettiTVA: boolean;
            isFranchiseEnBase: boolean;
            isRegimeMargeBeneficiaire: boolean;
            isAutoliquidation: boolean;
            numeroTVA: string;
            capital: number;
            isCapitalVariable: boolean;
            tauxPenalitesReglement: number;
            conditionsEscompte: string;
            mentionsParticulieres: string;
            modeReglementDefaut: string;
            private _personne;
            personne: Personne;
            private _identification;
            identification: Identification;
        }
    }
}
declare module QreaFacturation {
    module Models {
        class Identification extends BaseModel {
            constructor(params: any);
            SIREN: string;
            NIC: string;
            APE: string;
            RCS: string;
            RM: string;
            SIRET: string;
        }
    }
}
declare module QreaFacturation {
    module Models {
        class Logo extends BaseModel {
            constructor(params: any);
            base64: string;
        }
    }
}
declare module QreaFacturation {
    module Models {
        abstract class Personne extends BaseModel {
            constructor(params: any);
            static instanciatePhysiqueOuMorale(newPersonne: any): any;
            private _adresse;
            adresse: Adresse;
            isEntreprise: boolean;
            numeroTVA: string;
            telephone: string;
            email: string;
            siteInternet: string;
            fax: string;
            identification: Identification;
        }
        class PersonnePhysique extends Personne {
            constructor(params: any);
            civilite: string;
            nom: string;
            prenom: string;
            nomCommercial: string;
        }
        class PersonneMorale extends Personne {
            constructor(params: any);
            forme: string;
            denominationSociale: string;
            capitalSocial: number;
            isCapitalVariable: string;
        }
    }
}
declare module QreaFacturation {
    module Models {
        class Reglement extends BaseModel {
            constructor(params: any);
            delai: number;
            montant: number;
            pourcentage: number;
            finDeMois: boolean;
            paye: boolean;
        }
    }
}
declare module QreaFacturation {
    module Models {
        class Vente extends BaseModel {
            constructor(params: any);
            private calculate();
            private _article;
            article: IArticle;
            private _quantite;
            quantite: number;
            prctRemise: number;
            private _totalHT;
            totalHT: number;
            private _totalTTC;
            totalTTC: number;
            private _totalTVA;
            totalTVA: number;
        }
    }
}
declare var module: any;
