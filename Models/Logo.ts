
/// <reference path="../QreaFacturation.ts" />
/// <reference path="Models.ts" />

module QreaFacturation {

  export module Models {

    export class Logo extends BaseModel {

      constructor(params: any) {

        super(params);

        this.base64 = params.base64 || "";

      }

      base64: string;
      
    }

  }

}
