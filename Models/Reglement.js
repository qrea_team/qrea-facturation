var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        class Reglement extends Models.BaseModel {
            constructor(params) {
                super(params);
                this.delai = params.delai;
                this.montant = params.montant;
                this.pourcentage = params.pourcentage;
                this.finDeMois = params.finDeMois;
                this.paye = params.paye;
            }
        }
        Models.Reglement = Reglement;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
