var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        class Entreprise extends Models.BaseModel {
            constructor(params) {
                super(params);
                this.personne = params.personne;
                this.isAdherentCGA = params.isAdherentCGA;
                this.isExonere = params.isExonere;
                this.isAssujettiTVA = params.isAssujettiTVA;
                this.isFranchiseEnBase = params.isFranchiseEnBase;
                this.isRegimeMargeBeneficiaire = params.isRegimeMargeBeneficiaire;
                this.isAutoliquidation = params.isAutoliquidation;
                this.identification = params.identification;
                this.numeroTVA = params.numeroTVA;
                this.capital = params.capital;
                this.isCapitalVariable = params.isCapitalVariable;
                this.tauxPenalitesReglement = params.tauxPenalitesReglement;
                this.conditionsEscompte = params.conditionsEscompte;
                this.mentionsParticulieres = params.mentionsParticulieres;
                this.logoFacture = params.logoFacture;
                this.modeReglementDefaut = params.modeReglementDefaut;
            }
        }
        Models.Entreprise = Entreprise;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
