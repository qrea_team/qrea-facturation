var QreaFacturation;
(function (QreaFacturation) {
    var Models;
    (function (Models) {
        class Identification extends Models.BaseModel {
            constructor(params) {
                super(params);
                this.SIREN = params.SIREN;
                this.NIC = params.NIC;
                this.APE = params.APE;
                this.RCS = params.RCS;
                this.RM = params.RM;
                this.SIRET = this.SIRET + this.NIC;
            }
        }
        Models.Identification = Identification;
    })(Models = QreaFacturation.Models || (QreaFacturation.Models = {}));
})(QreaFacturation || (QreaFacturation = {}));
